#define MyAppName "Maller Complexity Scale"
#define MyAppVersion "1.0.0"
#define MyAppPublisher ""
#define MyAppURL "https://boostyourbim.wordpress.com/2016/07/26/mallercomplexity-family-tool-goes-open-source/"

#define RevitAddinFolder "{userappdata}\Autodesk\REVIT\Addins"

#define RevitAddin16  RevitAddinFolder+"\2016\"
#define RevitAddin17  RevitAddinFolder+"\2017\"
#define RevitAddin18  RevitAddinFolder+"\2018\"
#define RevitAddin19  RevitAddinFolder+"\2019\"
#define RevitAddin20  RevitAddinFolder+"\2020\"
#define RevitAddin21  RevitAddinFolder+"\2021\"

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{126699C2-6E49-429D-BF00-9C4B5EEEEB9F}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={pf}\{#MyAppName}
DisableDirPage=yes
DisableProgramGroupPage=yes
LicenseFile=.\LICENSEFREE
OutputDir=.
OutputBaseFilename=MallerComplexity.v{#MyAppVersion}
Compression=lzma
SolidCompression=yes
;info: http://revolution.screenstepslive.com/s/revolution/m/10695/l/95041-signing-installers-you-create-with-inno-setup
;comment/edit the line below if you are not signing the exe with the CASE pfx
;SignTool=signtoolcase

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Components]
Name: revit16; Description: Maller Complexity Scale for Autodesk Revit 2016;  Types: full
Name: revit17; Description: Maller Complexity Scale for Autodesk Revit 2017;  Types: full
Name: revit18; Description: Maller Complexity Scale for Autodesk Revit 2018;  Types: full
Name: revit19; Description: Maller Complexity Scale for Autodesk Revit 2019;  Types: full
Name: revit20; Description: Maller Complexity Scale for Autodesk Revit 2020;  Types: full
Name: revit21; Description: Maller Complexity Scale for Autodesk Revit 2021;  Types: full

[Files]

Source: "MallerComplexity.addin"; DestDir: "{#RevitAddin18}"; Flags: ignoreversion; Components: revit18

;REVIT 2016 ~~~~~~~~~~~~~~~~~~~
Source: "2018\*"; DestDir: "{#RevitAddin16}"; Excludes: "*.pdb,*.xml,*.config,*.addin,*.tmp"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: revit16 
Source: "MallerComplexity.addin"; DestDir: "{#RevitAddin16}"; Flags: ignoreversion; Components: revit16  

;REVIT 2017 ~~~~~~~~~~~~~~~~~~~
Source: "2018\*"; DestDir: "{#RevitAddin17}"; Excludes: "*.pdb,*.xml,*.config,*.addin,*.tmp"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: revit17   
Source: "MallerComplexity.addin"; DestDir: "{#RevitAddin17}"; Flags: ignoreversion; Components: revit17  

;REVIT 2018 ~~~~~~~~~~~~~~~~~~~
Source: "2018\*"; DestDir: "{#RevitAddin18}"; Excludes: "*.pdb,*.xml,*.config,*.addin,*.tmp"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: revit18 
Source: "MallerComplexity.addin"; DestDir: "{#RevitAddin18}"; Flags: ignoreversion; Components: revit18  

;REVIT 2019 ~~~~~~~~~~~~~~~~~~~
Source: "2019\*"; DestDir: "{#RevitAddin19}"; Excludes: "*.pdb,*.xml,*.config,*.addin,*.tmp"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: revit19 
Source: "MallerComplexity.addin"; DestDir: "{#RevitAddin19}"; Flags: ignoreversion; Components: revit19 

;REVIT 2020 ~~~~~~~~~~~~~~~~~~~
Source: "2020\*"; DestDir: "{#RevitAddin20}"; Excludes: "*.pdb,*.xml,*.config,*.addin,*.tmp"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: revit20
Source: "MallerComplexity.addin"; DestDir: "{#RevitAddin20}"; Flags: ignoreversion; Components: revit20 

;REVIT 2021 ~~~~~~~~~~~~~~~~~~~
Source: "2021\*"; DestDir: "{#RevitAddin21}"; Excludes: "*.pdb,*.xml,*.config,*.addin,*.tmp"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: revit21
Source: "MallerComplexity.addin"; DestDir: "{#RevitAddin21}"; Flags: ignoreversion; Components: revit21

; NOTE: Don't use "Flags: ignoreversion" on any shared system files
